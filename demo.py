my_info_dict = {
    "name": ["Pari", "Rakesh", "Hari"],
    "age": [41, 20, 22],
    "city": ["Chennai", "Bangalore", "Chennai"]
}

import pandas as pd

df = pd.DataFrame(my_info_dict)
df_shape = df.shape
print("df_shape of source data: {}".format(df_shape))

#print("Showing Pari's information: ")
#print(my_info_dict["name"][0])
#print(my_info_dict["age"][0])

class User:
    def __init__(self, name, age, city):
        self.name = name
        self.age = age
        self.city = city

    def teach(self, topic):
        print("{} is teaching topic/programing {}".format(self.name, topic))


user1 = User("Pari", 41, "Chennai")
user2 = User("Hari", 22, "Chennai")


print(user1)
print(user1.name)
print(user1.age)
print(user1.city)
user1.teach("Python")

print(user2.name)
print(user2.age)
print(user2.city)

print(type(my_info_dict))


nums = [1, 2, 3, 4, 5]

nums_2 = [6, 7, 8, 9, 10]

nums.append("11")
print(nums)
print(nums_2)