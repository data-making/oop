from os.path import join

class MyFile:
    file_name = ""
    root_path = ""

    def file_read(file_path):
        # 
        #
        file_object = open(file_path)

        return file_object
    
    def file_write(file_path, file_object):
        # 
        #
        file_object.write()
        file_status = True
        
        return file_status
    

class MyCSVFile(MyFile):

    def read_csv(self, file_object):
        file_content = file_object.read()
        print(file_content)

file_object = MyCSVFile()
file_object.file_name = "users.csv"
file_object.root_path = "/home/techlaps"

# /home/techlaps/users.csv

csv_file_object = file_object.read_csv(join(file_object.root_path, file_object.file_name))

file_object.read_csv(csv_file_object)
